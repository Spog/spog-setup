#!/bin/bash
apt-get update
apt-get upgrade

apt-get -y install software-properties-common
apt-add-repository -y ppa:ansible/ansible
apt-get update
apt-get -y install ansible
apt-get -y install git

echo "localhost ansible_connection=local" > /etc/ansible/hosts

mkdir -p /var/lib/ansible/local

# Cron job to git clone/pull a repo and then run locally
echo "*/15 * * * * root ansible-pull -d /var/lib/ansible/local -U https://Spog@bitbucket.org/Spog/spog-ansible-automation.git >> /var/log/ansible-pull.log 2>&1" > /etc/cron.d/ansible

echo -e "/var/log/ansible-pull.log {\n  rotate 7\n  daily\n  compress\n  missingok\n  notifempty\n}" > /etc/logrotate.d/ansible-pull
